#! /usr/bin/bash
#
#

for t in ../tex/*.tex ../tex/**/*.tex
do 
    echo $t
    grep '\\includegraphics' $t | \
        sed -e 's/.*\\includegraphics\b.*{\(.*\)}.*/\1/' | \
        sed -e 's/[^[:alnum:]]$//'
done | sort | uniq | xargs -i basename {} > included_images.txt

while read f; do find ~/Pictures/ -name "$f*" -exec cp -vu "{}" ../tex/images/ \; ; done < included_images.txt

