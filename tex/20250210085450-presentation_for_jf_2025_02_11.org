:PROPERTIES:
:ID:       7d56a5d4-0ca4-4059-8d49-517bebd4a0bb
:END:
#+title: Knowledge Graph(s) for NFDI4BIOIMAGE
#+shorttitle: NFDI4BI-KG
#+OPTIONS: num:nil toc:nil
#+REVEAL_TRANS: None
#+REVEAL_THEME: Solarized
#+Author: Carsten Fortmann-Grote
#+Mastodon: @c4r510
#+DATE:      February 11 2025
#+DESCRIPTION: 
#+KEYWORDS: NFDI, Bio Imaging, Linked Data, Controlled Vocabulary, Knowledge Base
#+LANGUAGE:  en
#+OPTIONS:   H:1 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t email:nil
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:https://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+HTML_LINK_UP:
#+HTML_LINK_HOME:
#+startup: beamer
#+LaTeX_CLASS: beamer
#+BEAMER_FRAME_LEVEL: 2
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)
#+latex_header: \mode<beamer>{\usetheme{Madrid}}
#+latex_header: \usepackage[url=false, eprint=false, style=authoryear,maxcitenames=1]{biblatex}
#+latex_header: \addbibresource{jabref.bib}
#+latex_header: \AtEveryBibitem{\clearfield{url}}
#+latex_header: \usepackage{tikz}
#+latex_header: \usetikzlibrary{shapes.geometric}
#+latex_header: \usetikzlibrary{positioning}
#+latex_header: \usetikzlibrary{calc}
#+latex_header: \usetikzlibrary{arrows}
#+latex_header: \usepackage{hyperref}
#+latex_header: \usepackage{hyperxmp}
#+latex_header: \hypersetup{pdfauthor={Carsten Fortmann-Grote},
#+latex_header: pdftitle={Knowledge Graph(s) for NFDI4BIOIMAGE},
#+latex_header: pdfkeywords={NFDI, Bio imaging, Linked Open Data, Metadata Standards, Ontop, Virtualization},
#+latex_header: pdfsubject={NFDI4BIOIMAGE TA1&3 Jour Fixe},
#+latex_header: pdfcreator={Emacs 28.2 (Org mode 9.7.4)},
#+latex_header: pdflang={English},
#+latex_header: pdfcontactemail={carsten.fortmann-grote@evolbio.mpg.de},
#+latex_header: pdfpubtype={presentation},
#+latex_header: pdflicenseurl={https://creativecommons.org/licenses/by/4.0/deed.en},
#+latex_header: pdfcopyright={Max-Planck-Gesellschaft zur Förderung der Wissenschaften e.V.},
#+latex_header: pdfdate={2025-02-11}
#+latex_header: }
#+latex_header: \input{resources/colors.tex}
#+latex_header: \setbeamercolor*{frametitle}{fg=white,bg=MPGGreen}
#+latex_header: \setbeamercolor*{section in head/foot}{fg=white,bg=MPGGreen}
#+latex_header: \setbeamercolor*{date in head/foot}{fg=white,bg=MPGGreen}
#+latex_header: \setbeamercolor*{author in head/foot}{fg=white,bg=MPGGreen}
#+latex_header: \setbeamercolor*{title in head/foot}{fg=white,bg=MPGGreen}
#+latex_header: \setbeamercolor{structure}{bg=MPGGreen}
#+latex_header: \setbeamercolor{block title}{fg=white,bg=structure.bg}
#+latex_header: \setbeamercolor{block body}{use=block title,bg=block title.bg!10}
#+latex_header: \setbeamercolor{title}{fg=white,bg=MPGGreen}
#+latex_header: \setbeamercolor{itemize item}{fg=MPGGreen}
#+latex_header: \setbeamercolor{itemize subitem}{fg=MPGGreen}
#+latex_header: \setbeamercolor{enumerate item}{fg=MPGGreen}
#+latex_header: \setbeamercolor{enumerate subitem}{fg=MPGGreen}
#+latex_header: \setbeamercolor{description item}{fg=MPGGreen}
#+latex_header: \setbeamertemplate{itemize item}[circle]
#+latex_header: \setbeamertemplate{itemize subitem}[circle]
#+latex_header: \setbeamertemplate{itemize subsubitem}[circle]
#+latex_header: \setbeamercolor{alerted text}{fg=MPGOrange,bg=MPGLightGreen}


* Knowledge Graph Construction
:PROPERTIES:
:ID:       f34c6145-c69d-4281-8c75-4cbd8a7208aa
:END:
** General scheme                                                   :B_frame:
:PROPERTIES:
:ID:       a06aa89e-c504-407c-ae1d-9ea475bfa75a
:BEAMER_env: frame
:END:
*** Virtual knowledge graph creation with Ontop-VKG                 :B_block:
:PROPERTIES:
:BEAMER_env: block
:ID:       8b6ec65c-c328-4fe4-a120-0c3a2eee8d89
:END:
#+attr_latex: :height .5\textheight
[[file:/home/grotec/Repositories/presentations/tex/img/vkg_vs_matkg.drawio.pdf]]
#+beamer:{\tiny
\fullcite{Calvanese2015}
#+beamer:}
***                                                                :noexport:
:PROPERTIES:
:ID:       dde9be58-8b77-402d-b6e3-85ae0d96df1a
:END:
#+attr_latex: :width 1cm
[[file:images/CC-BY-SA-4.0_88x31.png]]
** OMERO KG                                                         :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:ID:       c936ade5-a2aa-4dc7-b612-c5dd7994dbc5
:END:
:PROPERTIES:
:beamer_e
:ID:       f8d31cb7-f1dc-44e8-b06b-4ca5536678e9
:END:
*** Annotated images in OMERO                                       :B_block:
:PROPERTIES:
:BEAMER_env: block
:beamer_col: .49
:beamer_opt: t
:ID:       c9fa5975-458a-422e-ba75-8ae4cdaf7eb3
:END:
#+attr_latex: :width \columnwidth
[[/home/grotec/Repositories/presentations/tex/img/Screenshot_2025-01-27_14-31-21.png]]
*** Derived RDF Graph                                               :B_block:
:PROPERTIES:
:BEAMER_env: block
:beamer_col: .49
:beamer_opt: t
:ID:       e40a33f5-d28c-4fdb-a497-daf5b412c78d
:END:
#+attr_latex: :width \columnwidth
[[/home/grotec/Repositories/presentations/tex/img/ome_image_41697_graph-crop.pdf]]
** Towards a NFDI4BIOIMAGE KG                                       :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:ID:       ceb7cdfc-d893-4542-8e54-0023c3d4a728
:END:
- Generate a KG for all public OMEROs in NFDI4BIOIMAGE
- KGs from public OME-Zarr objects
- KGs for projects, use cases
- Harvest these KGs for summary metadata:
  - Number of instances
  - Number of users/project/datasets/images/Bytes per instance
  - types of images
  - Number of KV/tag/File/Table annotations
  - Unique keys
  - ...
- (Serve all KGs in a public SPARQL resource)
- Inject harvested data into Wikidata, ($=>$ NFDI4BIOIMAGE's entry =wd:Q113500855=)
** NFDI4BIOIMAGE @ Wikidata                                         :B_frame:
:PROPERTIES:
:ID:       d7fbb0a2-9fdf-4db8-ab4c-c99450782823
:BEAMER_env: frame
:END:

***                                                                 :B_block:
:PROPERTIES:
:BEAMER_env: block
:ID:       5125515c-4d12-4e30-b0ff-4e0f7031f0f4
:END:
#+beamer:{\tiny%
| name                         |               orcid | employer ror |
|------------------------------+---------------------+--------------|
| Astrid Schauss               |                     | 00rcxh774    |
| Markus Becker                |                     | 004hd5y14    |
| Claire Chalopin              | 0000-0001-9309-7531 | 03s7gtk40    |
| Jianxu Chen                  |                     | 02jhqqg57    |
| Johannes Wessels             |                     | 00pd74e08    |
| Edith Heard                  | 0000-0001-8052-7117 | 03mstc592    |
| Petra Hätscher               |                     | 0546hnb39    |
| Kerstin Krieglstein-Unsicker | 0000-0002-2130-7314 | 01jdpyv68    |
| Ursula Staudinger            | 0000-0003-3833-4340 | 042aqky30    |
| Jan U. Lohmann               | 0000-0003-3667-187X | 022jc0g24    |
| Jan Huisken                  | 0000-0001-7250-3756 | 01y9bpm73    |
| Thomas Bocklitz              | 0000-0003-2778-6624 |              |
| Anja Steinbeck               |                     | 00rcxh774    |
| Paul Czodrowski              | 0000-0002-7390-8795 | 01k97gp34    |
| Josh Moore                   | 0000-0003-4028-811X | 05tpnw772    |
| Jan Bumberger                |                     | 000h6jb29    |
| Susanne Menzel               |                     | 04qmmjx98    |
| Jean-Marie Burel             | 0000-0002-1789-1861 | 012a77v79    |
| Michael Baumann              | 0000-0002-9340-974X | 01zgy1s35    |
| Stefan Remy                  |                     | 01zwmgk08    |
| Axel A Brakhage              | 0000-0002-8814-4193 | 055s37c97    |
| Stefanie Weidtkamp-Peters    | 0000-0001-7734-3771 | 024z2rq82    |
| Carsten Fortmann-Grote       | 0000-0002-2579-5546 | 0534re684    |
| Timo Dickscheid              | 0000-0002-9051-3701 | 02nv7yv05    |
#+beamer:}

** Benefits                                                         :B_frame:
:PROPERTIES:
:ID:       9950fa46-1730-45ac-bdb1-9200c5fd9dbb
:BEAMER_env: frame
:END:
*** for Participants and Affiliations                               :B_block:
:PROPERTIES:
:BEAMER_env: block
:ID:       f6b813f6-1e36-4b6e-b13f-b2293d845a51
:END:
#+beamer:{\tiny
- Visibility of projects and activities in and beyond NFDI4BIOIMAGE
- Accessibility of data/projects/use case metadata
- Contribute to and early adopt metadata standardization
- Align activities with consortium mission
#+beamer:}
#+beamer:\pause
*** for NFDI4BIOIMAGE                                               :B_block:
:PROPERTIES:
:BEAMER_env: block
:ID:       c6562568-f0a8-4d60-8fdb-891547782a7b
:END:
#+beamer:{\tiny
- Common use case, integrates all institutions, projects and use cases
- Forces us to work on metadata standardization
- Enhance visibility in NFDI Sections and Workgroups
- Machine actionable metadata resource
- Knowledge base for Helpdesks, Data Stewards
#+beamer:}
#+beamer:\pause
*** for the Bioimage community                                      :B_block:
:PROPERTIES:
:BEAMER_env: block
:ID:       72a41d15-a4b0-44d3-8d87-df0b94fe886b
:END:
#+beamer:{\tiny
- Unified source of information about public bioimage resources
- Based on open web standards and protocols
#+beamer:}
** Howto derive a KG from a public OWERO instance          :B_frame:noexport:
:PROPERTIES:
:BEAMER_env: frame
:ID:       02eca2f0-c790-4158-bb8a-b2eac13b191e
:END:
- Clone [[https://github.com/German-BioImaging/omero-ontop-mappings][omero-ontop-mappings repo]]
- Install =ontop-cli=
- Setup read-only OMERO DB user
- Adjust RDF prefix for OMERO instance
- Launch =ontop endpoint=
