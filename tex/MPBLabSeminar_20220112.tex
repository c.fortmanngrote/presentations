% Created 2022-01-11 Tue 12:14
% Intended LaTeX compiler: pdflatex
\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{hyperxmp}
\usetheme{default}
\author{Carsten Fortmann-Grote}
\date{Jan. 12 2022}
\title{From databases to knowledge graph for Pseudomonas fluorescens SBW25}
\hypersetup{
 pdfauthor={Carsten Fortmann-Grote},
 pdftitle={From databases to knowledge graph for Pseudomonas fluorescens SBW25},
 pdfkeywords={linked open data, tripal, SPARQL, genome database, micropop},
 pdfsubject={MPB Department Seminar},
 pdfdate={D:20220112090000Z},
 pdfcreator={Emacs 27.1 (Org mode 9.4.6)}, 
 pdflang={English}
}
\begin{document}

\maketitle


\begin{frame}[label={sec:org046c05c}]{Introduction}
\begin{itemize}
\item Reference genomes and annotations sit in dedicated databases.
\item[$\Leftrightarrow$] Continuous growth of data and knowledge about the organism
\item Information soon outdated, inaccurate, misleading or wrong.
\item Updating reference genome data is a cumbersome endeavour (more about that later)

\item[$\Rightarrow$] Need a \alert{knowledge management system} where updated
  information is automatically integrated and served to the public or authorized clients.
\begin{itemize}
\item Genome sequence
\item Annotation tracks
\item *omics databases
\item Literature
\item Internal resources (ELNs, strain database, omero, \ldots{})
\item Generic data repositories (zenodo, figshare, \ldots{})
\item Social media
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org27c5ecd}]{Outline}
\begin{itemize}
\item Introduction\\[2ex]
\item A new assembly for Pseudomonas fluorescens SBW25\\[2ex]
\item Annotation of the new assembly\\[2ex]
\item Online genome browser for SBW25\\[2ex]
\item Connecting heterogeneous data sources into a knowledge graph\\[2ex]
\end{itemize}
\end{frame}


\begin{frame}[label={sec:orgdf4854f}]{A new assembly for Pseudomonas fluorescens SBW25}
\begin{itemize}
\item Whole genome sequencing project [Joanna Summers]
\begin{itemize}
\item PacBio SMRT platform
\item 600x coverage

\end{itemize}
\end{itemize}
\begin{center}
  \includegraphics[width=.9\linewidth]{2022-01-10_09-07-05_screenshot.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:orgcde0da3}]{Strain database entries}
\begin{center}
  \includegraphics[width=.9\linewidth]{2022-01-11_16-21-12_screenshot.png}
  \includegraphics[width=.9\linewidth]{2022-01-11_16-22-55_screenshot.png}
\includegraphics[width=.9\linewidth]{2022-01-10_09-14-30_screenshot.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:org5a72556}]{Assembly [Erik Hugoson, Loukas Theodosiou]}
\begin{itemize}
\item Software: Spades 3.15.3
\item Output: 1 chromosome of 6722400 bp (old reference genome:  6277539)
\item Alignment  \includegraphics[width=.7\linewidth]{2022-01-10_10-57-24_screenshot.png}
\item Differences: 3 Indels in repeat regions, 4 SNPs in rRNAs.
\end{itemize}
\begin{center}
\includegraphics[width=.7\linewidth]{2022-01-10_09-28-05_screenshot.png}
\end{center}
\begin{itemize}
\item Identical results obtained with various whole genome alignment tools
\end{itemize}
\end{frame}



\begin{frame}[label={sec:orga8d5853}]{Annotation transfer}
\begin{itemize}
\item Which annotation?
\item There are multiple "reference" annotations around.
\item The original (manual) annotation by Sanger (2009) and various versions of GCA000009225.1 on NCBI.
\item They differ in locus tag prefixes, number of genes vs. pseudogenes, functional annotations.
\item We considered these annotations:
  \begin{itemize}
    \item \alert{Sanger2009} Manual annotation for the original assembly 
    \item \alert{ENSEMBL2015} $\hat{=}$  Genbank: GCA\_000009225.1 $\hat{=}$
      Refseq GCF\_000009225.2
  \end{itemize}                     
\item We used the ``Transfer annotation'' function in Geneious-prime and
  \item manually merged both transfers into one gff3 file.
\end{itemize}
\end{frame}

\begin{frame}{Annotation differences}
\begin{itemize}
\item Example to illustrate differences: The wssH gene
  \begin{center}
    \includegraphics[width=.9\linewidth]{2022-01-10_11-54-45_screenshot.png}
  \end{center}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org8cf8735}]{Annotation merging}
\begin{itemize}
\item Use \alert{ENSEMBL2015} as basis
\item \alert{genes}: copy from ENSEMBL2015
\item \alert{CDS}: join ENSEMBL2015 and Sanger2009, add \textit{dbxrefs} and \textit{products} from UniProt
\item \alert{exons}: copy from ENSEMBL2015
\item \alert{mRNA}: copy ENSEMBL2015
\item \alert{tRNA}, rRNA: take ENSEMBL2015 and copy extra entries from Sanger2009
\item \alert{all other features}:
\begin{itemize}
\item check if feature at same position exists, merge if yes, copy if only exists in one.
\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}[label={sec:org318ff7b}]{Data deposition}
\begin{itemize}
\item Updating original assembly / annotation can only be done by NCBI/ENSEMBL account owner, that person did not reply.
\item Drafted a new submission and will request suppression of previous entry
\item New locus tag needs to be assigned: MPB\_XXXXX, 5 digits interspacing to allow for subsequent updates
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgc89e0f1}]{The SBW25 genome browser}
In the meantime, I have created a SBW25 genome database and browser at \url{http://micropop046.evolbio.mpg.de/sbw25}

\begin{center}
\includegraphics[width=.9\linewidth]{2022-01-10_12-12-09_screenshot.png}
\end{center}
\end{frame}
\begin{frame}
  \frametitle{JBrowse instance for the SBW25 genome database}
  \includegraphics[width=.9\linewidth]{2022-01-12_08-44-36_screenshot.png}
\end{frame}

\begin{frame}[label={sec:orge3d5763}]{Integration of further data sources}
\begin{block}{Keeping the information up to date}
\end{block}
\begin{block}{Integrating external and internal resources}
\end{block}
\end{frame}


\begin{frame}[label={sec:org08c7d45}]{Linked data and semantic web technology}
\begin{itemize}
\item Most (if not all) biological data is organized in relational databases
  (RDBs) and exposed via web frontends. 
\item In many cases (but not in all cases), there exist dedicated APIs for programmatic access. One notable example \alert{without}
API is pseudomonas.com :(
\item Some (far from all) providers even offer a standardized API, a so called "SPARQL" endpoint (e.g. UniProt, KEGG).
\begin{itemize}
\item SPARQL stands for "SPARQL Protocol And RDF Query Language".
\item RDF stands for "Resource Description Format".
\item RDF supports semantic modeling of data, i.e. describing the type, properties,
and values of entities as well as their relations.
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}{Triples = Statements about data}
\begin{center}
\includegraphics[width=.9\linewidth]{2022-01-10_12-47-08_screenshot.png}
\end{center}
\end{frame}
\begin{frame}{}
\begin{itemize}
\item A subject can have multiple "S-P-O" relations
\begin{center}
\includegraphics[width=.9\linewidth]{2022-01-10_12-53-19_screenshot.png}
\end{center}
\end{itemize}
\end{frame}
\begin{frame}{}
\begin{itemize}
\item An object can become the subject in another relation ("sentence")
\end{itemize}

\begin{center}
\includegraphics[width=.9\linewidth]{2022-01-10_12-55-26_screenshot.png}
\end{center}

\end{frame}
\begin{frame}{}
\begin{itemize}
\item Example for genomic data
\end{itemize}
\begin{center}
\includegraphics[width=.9\linewidth]{2022-01-10_12-57-40_screenshot.png}
\end{center}

\end{frame}
\begin{frame}{}

\begin{itemize}
\item SPARQL the query language for RDF (SPARQL is to RDF what SQL is to
  relational DBs (RDBs)).
\item RDF Triple stores and SQL DBs are equivalent, SPARQL queries can be translated 1:1 into SQL
  queries and vice-versa.
\item \alert{But} SQL cannot query separate databases, whereas SPARQL can query various
  triplestores in one operation.
\item This makes it possible to query even separate RDBs with SPARQL.
\item RDF schemes are formalized ontologies, defined in the same syntax as the data itself $\Rightarrow$ RDF data is self-describing.
\item RDF and SPARQL are centralized web standards (like HTTP and HTML) published by the W3C consortium.
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org74038eb}]{Linking heterogeneous data sources for Pseudomonas fluorescens SBW25.}
\begin{center}
\includegraphics[width=.9\linewidth]{ICSB2021_Poster694_FortmannGrote_final.pdf}
\end{center}
\end{frame}
\begin{frame}
  \frametitle{Querying an RDF graph}
  \begin{center}
    \includegraphics[width=.9\linewidth]{sparql_query_bdc_figure1.1.png}
  \end{center}
  [Bod DuCharme ``Learning SPARQL'', O'Reilly (2013)]
\end{frame}

\begin{frame}[label={sec:orgf811b1d}]{Querying the SBW25 genome database}
\begin{itemize}
\item SQL data mapped to RDF
\end{itemize}

\begin{center}
\includegraphics[width=.9\linewidth]{2022-01-10_14-36-57_screenshot.png}
\end{center}
\end{frame}

\begin{frame}{}
\begin{block}{Query all features}
\begin{center}
\includegraphics[width=.9\linewidth]{2022-01-10_14-44-42_screenshot.png}
\end{center}
\end{block}
\end{frame}

\begin{frame}[label={sec:orgc7b62fc}]{Query all features and their names}
\begin{center}
\includegraphics[width=.9\linewidth]{2022-01-10_14-46-27_screenshot.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:orgcec41ba}]{Filter one specific feature (wssH CDS)}
\begin{center}
\includegraphics[width=.9\linewidth]{2022-01-10_14-51-33_screenshot.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:org0fe135f},fragile]{Federated query to uniprot}
  \tiny
 \begin{verbatim}
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX chado: <http://purl.org/net/chado/schema/>
PREFIX up: <http://purl.uniprot.org/core/> 
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

select ?protein ?protein_prop ?protein_val where {
    {
        service <http://micropop046.evolbio.mpg.de:8080/sparql> {
            ?feat a chado:Feature .
            ?feat chado:name ?name .
            ?feat chado:uniquename ?uname .
            ?feat chado:type ?type .
            filter(?type=872) .
            filter(regex(?name, "^wssH-1$")) . 
        }
        bind(substr(?uname, 12) as ?transcript)
        bind(iri(concat("http://rdf.ebi.ac.uk/resource/ensembl.transcript/", ?transcript)) as ?emblcds_iri)

        service <http://sparql.uniprot.org/sparql> {
            ?protein a up:Protein ;
                    rdfs:seeAlso ?emblcds_iri ;
                    ?protein_prop ?protein_val .
       }
   }

}
\end{verbatim}
\end{frame}

\begin{frame}{}
  \begin{center}
    \includegraphics[width=.9\linewidth]{2022-01-11_14-05-10_screenshot.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:orga524905}]{MPB StrainDB}
The strainDB is served as a RDF triplestore at \url{http://seek2.evolbio.mpg.de:8000/sparql}

\begin{itemize}
\item Which wss* mutants are in the strain database, what are their MPB numbers and IDs in the genome database?
\end{itemize}

\begin{center}
\includegraphics[width=.9\linewidth]{2022-01-11_11-46-46_screenshot.png}
\end{center}
\end{frame}

\begin{frame}{Strain DB query results}

\begin{center}
\includegraphics[width=.9\linewidth]{2022-01-11_11-48-52_screenshot.png}
\end{center}
\pause
The strain DB website does not support this type of query.
\end{frame}

\begin{frame}{Query the genome DB}
  \begin{itemize}
    \item Get all features of type CDS, their feature ID, unique name, and gene name
  \end{itemize}

\begin{center}
\includegraphics[width=.9\linewidth]{2022-01-11_11-49-53_screenshot.png}
\end{center}

\end{frame}

\begin{frame}{Genome DB query results}

\begin{center}
\includegraphics[width=.9\linewidth]{2022-01-11_11-51-56_screenshot.png}
\end{center}

\end{frame}

\begin{frame}{Joined results}
\begin{center}
  \includegraphics[width=.9\linewidth]{2022-01-11_13-57-24_screenshot.png}
\end{center}
\end{frame}

\begin{frame}
  \frametitle{Querying evolutionary relationships from orthologue databases}
  \includegraphics[width=.9\linewidth]{10.12688_f1000research.21027.2_f6.pdf}
\end{frame}

\begin{frame}
  \frametitle{Summary}
  \begin{itemize}
  \item The new SBW25 assembly will replace the current reference data
  \item The merged annotation needs some final fixes
  \item Assembly and annotation are already accessible (VPN required)
  \item A SPARQL endpoint supports Linked Data access to the new genome
  \item Federated queries have been demonstrated between the genome DB and
    external and internal resources
  \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Outlook}
  \begin{itemize}
  \item Usability: Not everyone may want to learn SPARQL ...
    \begin{itemize}
    \item Graphical query builders
    \item AI translator
    \end{itemize}
    \item More resources (internal and external)
  \end{itemize}
\end{frame}
\end{document}