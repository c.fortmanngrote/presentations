\documentclass[final]{beamer}
\usepackage[orientation=portrait,size=a0,scale=2.0]{beamerposter}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usepackage{calc,amsmath,amssymb,amsfonts}
\usepackage{nameref}
\usepackage[sfdefault]{roboto}
\usepackage[LGR,T1]{fontenc}
\usepackage[german]{babel}
\usepackage[style=authoryear-icomp, backend=biber]{biblatex}
\usepackage{array,supertabular,hhline}
\usepackage[]{graphicx}
\usepackage{marvosym}
\usepackage{hyperref}
\usepackage[numberedsection=nameref]{glossaries}
\usepackage{hyperxmp}

\newcommand{\addheight}{\parbox{0pt}{\rule{0pt}{3cm}}}
% \setbeamerfont*{block title}{series=\bfseries\addheight,size=\semiHuge} 

\usetheme{Madrid}
\input{resources/colors}
\definecolor{mpievolbio_green}{RGB}{0,124,119}
\setbeamercolor*{palette primary}{bg=MPGGreen,fg=white}
\setbeamercolor{palette secondary}{bg=MPGGreen,fg=white}
\setbeamercolor{palette tertiary}{bg=MPGGreen,fg=white}
\setbeamercolor{palette quaternary}{bg=MPGGreen,fg=white}
\setbeamercolor{structure}{fg=MPGGreen} % itemize, enumerate, etc
\setbeamercolor*{frametitle}{fg=white,bg=MPGGreen}
\setbeamercolor*{section in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{date in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{author in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{title in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor{structure}{bg=MPGGreen}
\setbeamercolor{block title}{fg=white,bg=structure.bg}
\setbeamercolor{block body}{use=block title,bg=block title.bg!10}
\setbeamercolor{title}{fg=white,bg=MPGGreen}
\setbeamercolor{itemize item}{fg=MPGDarkGreen}
\setbeamercolor{itemize subitem}{bg=MPGDarkGreen}
\setbeamercolor{enumerate item}{bg=MPGGreen}
\setbeamercolor{enumerate subitem}{bg=MPGGreen}
\setbeamercolor{description item}{bg=MPGGreen}
\setbeamercolor{alerted text}{fg=MPGOrange,bg=MPGLightGreen}
\date{Digitale Woche Kiel, 15. Mai 2024}
\title[FDM]{Forschungsdaten am Max-Planck-Institut für Evolutionsbiologie}
\hypersetup{
  pdfauthor={Carsten Fortmann-Grote, Beate Gericke, Iben Martinsen, Kristian
    Ullrich, Derk Wachsmuth},
  pdfdate={D:20240515140000Z},
  pdftitle={Forschungsdaten am Max-Planck-Institut für Evolutionsbiologie},
  pdfsubject={Posterpräsentation bei der Digitalen Woche Kiel 2024},
  pdfkeywords={research data management, FAIR, openbis, omero, policy, library},
  pdfdoi={10.5281/zenodo.12199276},
  pdfcontactemail={carsten.fortmann-grote@evolbio.mpg.de},
 pdfpubtype={poster},
 pdflicenseurl={https://creativecommons.org/licenses/by-sa/4.0/deed.en},
 pdfcopyright={Max-Planck-Gesellschaft zur Förderung der Wissenschaften e.V.},
  pdflang={English}
}

\makeatother
\setbeamertemplate{footline}{}
\makeatletter
\setbeamertemplate{navigation symbols}{}
\addbibresource{jabref.bib}
\makeglossaries
\input{glossary.tex}

\setlength\tabcolsep{1mm}
\renewcommand\arraystretch{1.2}
\author[Fortmann-Grote et al.]{
  Carsten Fortmann--Grote,
  Beate Gericke,
  Iben Martinsen,
  Kristian Ullrich,
  Derk Wachsmuth
}
\begin{document}
\begin{frame}{}
  \maketitle
  \begin{columns}[T]
    %%%%%%%%%%%%%%%%%%%%%%%%%% 
    % Left major column
    %%%%%%%%%%%%%%%%%%%%%%%%%% 
    \begin{column}{.49\textwidth}
      \begin{block}{Grundsätze\addheight}
        \vspace*{1ex}
        \begin{itemize}
        \item Daten Management Plan zu Beginn eines Projekts
        \item Alle \textbf{Rohdaten sind in internen Datenbanken hinterlegt und dokumentiert}
          (OpenBIS, OMERO, GitLab)
        \item Rohdaten und Quellcode for \textbf{publizierte Resultate in
            offenen Repositorien hinterlegt}
        \end{itemize}
      \end{block}
      \begin{block}{Datenquellen\addheight}%
        \begin{columns}[T]
          \begin{column}{.33\textwidth}
            \begin{center}
              \includegraphics[width=.9\textwidth, viewport=50 100 300 300, clip]{Datasources_at_MPIEB.pdf}\\[-1ex]
              {\tiny (C) Zeiss}\\
              \textbf{Bilddaten}
              \begin{itemize}
              \item Mikroscopie
              \item CT, Röntgen
              \item Kameras
              \end{itemize}
            \end{center}
            \vspace*{\fill}
          \end{column}
          \begin{column}{.33\textwidth}
            \begin{center}
              \includegraphics[width=.9\textwidth, viewport=330 120 550 300, clip]{Datasources_at_MPIEB.pdf}\\[-1ex]
              {\tiny (C) Illumina}\\
              \textbf{Sequenzdaten}
              \begin{itemize}
              \item Intern
              \item Extern
              \end{itemize}
            \end{center}
            \vspace*{\fill}
          \end{column}
          \begin{column}{.33\textwidth}
            \begin{center}
              \includegraphics[width=.9\textwidth, viewport=580 130 800 300, clip]{Datasources_at_MPIEB.pdf}\\[-1ex]
              {\tiny (C) IBM}\\
              \textbf{Simulationen}
              \begin{itemize}
              \item Onsite
              \item Offsite
              \end{itemize}
            \end{center}
            \vspace*{\fill}
          \end{column}
        \end{columns}
      \end{block}
    \end{column}
    %%%%%%%%%%%%%%%%%%%%%%%%%% 
    % Right major column
    %%%%%%%%%%%%%%%%%%%%%%%%%% 
    \begin{column}{.49\textwidth}
      \begin{block}{Schema\addheight}
        \vspace*{.5ex}
        \begin{center}
          \includegraphics[width=.8\textwidth, clip]{data_flowchart.pdf}
        \end{center}
        \vspace*{-2ex}
        {\tiny \sloppy DMP=Daten Management Plan\hspace{3ex} FDK=Forschungsdatenkoordinator}
      \end{block}
      \begin{block}{Repositorien\addheight}
        \begin{tabular}{l|l|l|l|}
          &\textbf{Doku}&\textbf{Intern}&\textbf{Öffentlich}
          \\ \hline \textbf{Sequenzen} & OpenBIS & Seq. Archiv & SRA, ENA
          \\ \textbf{Mikroskopie}    & OMERO   & OMERO    & BioimageArchive
          \\ \textbf{Code}          & GitLab  & GitLab   & Github, Edmond
        \end{tabular}
      \end{block}
    \end{column}
  \end{columns}
  \vspace*{1ex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
  \begin{block}{Elektronisches Laborbuch und Labor-Informations-Management System OpenBIS\addheight}
    \vspace*{1ex}
    \begin{columns}
      \begin{column}{.49\textwidth}
        \begin{center}
          \includegraphics[width=.8\textwidth]{openbis_in_a_nutshell_crop}
        \end{center}
          \tiny\sloppy ETH Zürich
      \end{column}
      \begin{column}{.49\textwidth}
        \begin{columns}[T]
          \begin{column}{.49\textwidth}
            \begin{alertblock}{Laborbuch\addheight}%
              \begin{center}
                \includegraphics[width=\textwidth]{Screenshot_2024-04-23_21-01-28.png}
              \end{center}
            \end{alertblock}%
            \begin{alertblock}{Bakterienstämme\addheight}
              \begin{center}
                \includegraphics[width=\textwidth]{Screenshot_2024-04-23_21-05-04.png}
              \end{center}
            \end{alertblock}
          \end{column}
          \begin{column}{.49\textwidth}
            \begin{alertblock}{Formblatt Z\addheight}
              \begin{center}
                \includegraphics[width=\textwidth]{Screenshot_2024-04-23_21-10-57}
              \end{center}
            \end{alertblock}
            \begin{alertblock}{Sequenzieraufträge\addheight}
              \begin{center}
                \includegraphics[width=\textwidth]{Screenshot_2024-05-03_12-35-23}
              \end{center}
            \end{alertblock}
          \end{column}
        \end{columns}
      \end{column}
    \end{columns}
    \vspace*{1ex}
  \end{block}
\vspace*{1ex}
\begin{block}{Mikroskopie Datenbank OMERO\addheight}
  \begin{columns}
    \begin{column}{.49\textwidth}
      \begin{center}
        \includegraphics[width=\textwidth, clip, viewport=0 150 800 400]{omero_at_mpieb.pdf}
      \end{center}
    \end{column}
    \begin{column}{.49\textwidth}
      \begin{center}
        \begin{itemize}
        \item Daten per GUI importieren
        \item Key-Value Annotationen
        \item Webform
        \item Spreadsheet
        \item Kontrollierten Vokabularien
        \end{itemize}
      \end{center}
    \end{column}
  \end{columns}
\end{block}
\centering{%
  \includegraphics[width=0.05\textwidth]{CC-BY-SA-4.0_88x31}%
  \hspace*{2ex}%
  \tiny\sloppy Copyright: Max-Planck-Gesellschaft (2024)
}
\end{frame}
\end{document}
