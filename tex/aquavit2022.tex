\documentclass[25pt,a0paper]{tikzposter}
\usepackage{lipsum}
\usepackage{biblatex}
\addbibresource{jabref.db.bib}
\AtEveryBibitem{\clearfield{title}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{doi}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{issn}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{isbn}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{url}}{}}
%% Tikzposter is highly customizable: please see
%% https://bitbucket.org/surmann/tikzposter/downloads/styleguide.pdf

%% Available themes: see also
%% https://bitbucket.org/surmann/tikzposter/downloads/themes.pdf
\usetheme{Default}
% \usetheme{Rays}
% \usetheme{Basic}
% \usetheme{Simple}
% \usetheme{Envelope}
% \usetheme{Wave}
% \usetheme{Board}
% \usetheme{Autumn}
% \usetheme{Desert}

%% Further changes to the title etc is possible
% \usetitlestyle{Default}
% \usetitlestyle{Basic}
% \usetitlestyle{Empty}
% \usetitlestyle{Filled}
% \usetitlestyle{Envelope}
% \usetitlestyle{Wave}
% \usetitlestyle{verticalShading}


\author{Carsten Fortmann-Grote, Paul B. Rainey}
\title{From Genome Annotation to Knowledge Graph}
% \subtitle{The case of \textit{Pseudomonas fluorescens} SBW25}
\institute{Max Planck Institute for Evolutionary Biology\\
Dept. Microbial Population Biology}
%% Optional title graphic
%\titlegraphic{\includegraphics[width=7cm]{IMG_1934}}
%% Uncomment to switch off tikzposter footer
\tikzposterlatexaffectionproofoff

\begin{document}
\maketitle

\begin{columns}
\column{0.499}
% \block{Abstract}{
%   \noindent
% We have recently published an updated genome assembly and annotation of our
% model organism \textit{Pseudomonas fluorescens} SBW25. We are now facing the
% challenge to keep the annotation up to date with novel results from experimental
% and computational studies of gene function, fitness assays, regulatory and
% metabolic networks in a continuous, transparent, and accessible manner. In this contribution
% we will present how we combine various opensource
% software tools and open data and metadata standards into a public knowledge base for
% our model organism. The central part is our genome database and genome browser
% which is based
% on the opensource framework Tripal. It allows internal and external
% colleagues to feed in their data and results in a curated fashion.
% To further integrate our data
% we are working on a Linked Data architecture
% that connects our genome database to various *omics databanks such as UniProt,
% KEGG or Rfam, as well as to internal datasources such as our microscopy image database, strain
% database, sequence data repository, and data sharing platform to form an organism
% specific knowledge graph. By exposing a public SPARQL endpoint, our data
% ultimately becomes part of the world wide semantic web that incorporates other, domain
% specific knowledge graphs but also generic data sources such as Wikipedia (via WikiData) and social
% media hubs. In this way, our system facilitates the growth of
% the \textit{Pseudomonas fluorescens} SBW25 knowledge graph  both through manual
% explorations as well as through automated procedures.

% All components of our system are opensource products. We heavily benefit from
% open data and metadata standards and we strive to ``pay'' back to the
% opensource community by contributing customizations to the various software
% projects and by making our genome annotation ontology part of the
% public domain.
  
% }%
% %
\block{A SPARQL Endpoint for  \textit{Pseudomonas fluorescens} SBW25 data}{
  \begin{itemize}
  \item Lightweight \textbf{ontology} \cite{FortmannGrote2022a} to map gff3 attributes to RDF properties.
  \item Conversion of SBW25 genome from gff3 to RDF \cite{Klyne2004}
  \item Serving the \textit{Pseudomonas fluorescens} SBW25 \textbf{knowledge graph} via \textbf{SPARQL Endpoint}
  \end{itemize}
  \begin{center}
  \includegraphics[width=.8\colwidth]{Screenshot_2022-06-08_12-23-32}%
  \end{center}
}
\block{So what is SPARQL?}{%
  \begin{itemize}
  \item \textbf{S}PARQL \textbf{P}rotocol and \textbf{R}DF \textbf{Q}uery \textbf{L}anguage \cite{Prudhommeaux2008}
  \item Data is modeled as triples (\textbf{Subject}--\textbf{Predicate}--\textbf{Object})
  \item Predicates and Objects are defined in an \textbf{ontology} (think ``DB schema'')
  \item Triples form a graph of nodes (Subjects and Objects) and edges (Predicates)
  \item SPARQL finds all triples that match a pattern and returns them as a table
  \item R2RML \cite{Das2012} maps between SPARQL and SQL \cite{Calvanese2015}
    \item SPARQL supports \textbf{federated queries} across multiple independent triplestores
  \end{itemize}
  \begin{center}%
    \includegraphics[width=.8\colwidth, clip=True, viewport=0 50 800 400]{rdf_gene_tree_example-crop.pdf}
  \end{center}
}
\block{Example: Query UniProt annotations for all CDSs in the \textit{Pseudomonas fluorescens} SBW25 Knowledge Graph}{%
  \begin{center}
    \includegraphics[width=.8\colwidth]{Screenshot_2022-06-08_15-40-48}
  \end{center}
}
%
\column{0.499}
\block{A new genome for \textit{Pseudomonas fluorescens} SBW25}{%
  \begin{itemize}
  \item Whole genome resequencing of original \textit{Pseudomonas fluorescens} SBW25 sample (BioProject PRJEA31229)
  \item PacBio long read sequencing (ERR8265873) at 400x coverage plus Illumina short read data (ERR9795577)
  \item[$\Rightarrow$] High quality \textbf{hybrid assembly} (EBI:OV986001)
  \item \textbf{Manually merged annotation:} original Sanger (manual) annotation  plus most recent automatic annotation
    \item[$\Rightarrow$] High quality genome annotation (EBI:OV986001)
  \end{itemize}
}
\block{The \textit{Pseudomonas fluorescens} SBW25 database}{%
  \begin{itemize}
  \item Genome database and web frontend based on Chado \cite{Mungall2007} and Tripal \cite{Spoor2019}
  \item Genome browser, search interface, annotations and (active) cross-references
  \item Logged--in users can enrich annotations, data, analysis
  \item JSON API for programmatic access from R-Studio, Jupyter, Galaxy, ...
  \end{itemize}
  \begin{minipage}{\colwidth}%
    \mbox{}
    \begin{minipage}{.45\colwidth}%
  \includegraphics[width=.45\colwidth]{Screenshot_2022-06-08_12-41-18}\\[1ex]
  \includegraphics[width=.45\colwidth]{Screenshot_2022-06-08_12-42-50}
\end{minipage}%
\hspace{.02\colwidth}
\begin{minipage}{.43\colwidth}%
  \includegraphics[width=.43\colwidth]{Screenshot_2022-06-08_12-45-54}
\end{minipage}%
\end{minipage}
}
\block{Interfacing external DBs ...}{
  \begin{minipage}{1.0\colwidth}%
    \mbox{}%
    \begin{minipage}{0.3\colwidth}%
      \includegraphics[width=\textwidth]{Screenshot_2022-06-08_13-43-56}%
    \end{minipage}%
    \hspace*{.5cm}
    \begin{minipage}{0.3\colwidth}%
      \includegraphics[width=\textwidth]{Screenshot_2022-06-08_13-53-33}%
    \end{minipage}%
    \hspace*{.5cm}
    \begin{minipage}{0.3\colwidth}%
      \includegraphics[width=\textwidth]{Screenshot_2022-06-08_14-00-27}%
    \end{minipage}%
  \end{minipage}%
}%
\block{... and internal DBs}{
  \begin{minipage}{1.0\colwidth}%
    \mbox{}%
    \begin{minipage}{0.45\textwidth}%
      \includegraphics[width=\textwidth]{Screenshot_2022-06-08_14-45-26}%
    \end{minipage}%
    \hspace*{.05\textwidth}%
    \begin{minipage}{0.45\textwidth}%
      \includegraphics[width=\textwidth]{Screenshot_2022-06-08_14-47-26}%
    \end{minipage}%
  \end{minipage}%
} 
\block{}{%
  \printbibliography[heading=none]
}%
\end{columns}
\end{document}