\documentclass[final]{beamer}
\usepackage[orientation=portrait,size=a0,scale=2.0]{beamerposter}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usepackage{calc,amsmath,amssymb,amsfonts}
\usepackage{nameref}
\usepackage[sfdefault]{roboto}
\usepackage[LGR,T1]{fontenc}
\usepackage[english]{babel}
\usepackage[backend=biber,maxnames=1,sorting=none]{biblatex}
\usepackage{array,supertabular,hhline}
\usepackage[]{graphicx}
\usepackage{marvosym}
\usepackage{hyperref}
\usepackage[numberedsection=nameref]{glossaries}
\usepackage{hyperxmp}

\newcommand{\addheight}{\parbox{0pt}{\rule{0pt}{3cm}}}
% \setbeamerfont*{block title}{series=\bfseries\addheight,size=\semiHuge} 
% Adjust fontsize for bibliography
\renewcommand*{\bibfont}{\footnotesize}

\usetheme{Madrid}
\input{resources/colors}
\definecolor{mpievolbio_green}{RGB}{0,124,119}
\setbeamercolor*{palette primary}{bg=MPGGreen,fg=white}
\setbeamercolor{palette secondary}{bg=MPGGreen,fg=white}
\setbeamercolor{palette tertiary}{bg=MPGGreen,fg=white}
\setbeamercolor{palette quaternary}{bg=MPGGreen,fg=white}
\setbeamercolor{structure}{fg=MPGGreen} % itemize, enumerate, etc
\setbeamercolor*{frametitle}{fg=white,bg=MPGGreen}
\setbeamercolor*{section in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{date in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{author in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{title in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor{structure}{bg=MPGGreen}
\setbeamercolor{block title}{fg=white,bg=structure.bg}
\setbeamercolor{block body}{use=block title,bg=block title.bg!10}
\setbeamercolor{title}{fg=white,bg=MPGGreen}
\setbeamercolor{itemize item}{fg=MPGDarkGreen}
\setbeamercolor{itemize subitem}{bg=MPGDarkGreen}
\setbeamercolor{enumerate item}{bg=MPGGreen}
\setbeamercolor{enumerate subitem}{bg=MPGGreen}
\setbeamercolor{description item}{bg=MPGGreen}
\setbeamercolor{alerted text}{fg=MPGOrange,bg=MPGLightGreen}

% BIBLATEX
\addbibresource{jabref.bib}
\AtEveryBibitem{\clearfield{title}}
\AtEveryBibitem{\clearfield{urldate}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{issn}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{isbn}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{url}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{pages}}{}}

\usetheme{Madrid}

%%% XMP config
\usepackage{hyperxmp}
\usepackage{hyperref}
\hypersetup{
  pdfdate={D:20240704090000Z},
  pdfsubject={Aquavit 2024},
  pdfpubtype={Poster},
  pdfdoi={10.5281/zenodo.12636023}
}


\makeatother
\setbeamertemplate{footline}{}
\makeatletter
\setbeamertemplate{navigation symbols}{}

\usepackage{changepage}
\addtobeamertemplate{block begin}
  {}
  {\vspace{1ex plus 0.5ex minus 0.5ex} % Pads top of block
     % separates paragraphs in a block
   \setlength{\parskip}{4pt plus 1pt minus 1pt}%
   \begin{adjustwidth}{1cm}{1cm}
}
\addtobeamertemplate{block end}
  {\end{adjustwidth}%
   \vspace{1ex plus 0.5ex minus 0.5ex}}% Pads bottom of block
  {\vspace{1ex plus 1ex minus 1ex}} % Seperates blocks from each other

\addbibresource{jabref.bib}
\makeglossaries
\input{glossary.tex}

\setlength\tabcolsep{1mm}
\renewcommand\arraystretch{1.2}
\setbeamertemplate{caption}{\raggedright\insertcaption\par}

\author{Carsten Fortmann-Grote, Julia v. Irmer, Frederic Bertels}
\title{RAREFAN: The RAYT and REPIN Finder and Analysis Webservice}
\date{Aquavit 2024}
%% Optional title graphic
% \logo{\includegraphics[height=5cm]{mpieb_logo_FGwhite_BGalpha.png}}

\begin{document}
\begin{frame}
  \titlepage
  % \begin{block}{}
  %   \maketitle
  % \end{block}
\begin{columns}[T]
\begin{column}{0.485\textwidth}
\begin{block}{RAYTs and REPINs\addheight}
  \vspace*{1ex}
    \begin{center}
      \includegraphics[width=.8\textwidth]{FortmannGrote2023_Figure_1.pdf}
    \end{center}
    \begin{itemize}
    \item REPIN populations abundant in bacterial genomes~\cite{Bertels2011}
    \item RAYT: REPIN associated transposase (gene)
    \item Identification is laborious and complex 
    \item RAREFAN: Web based REPIN identification~\cite{FortmannGrote2023b} 
    \end{itemize}
    \vspace{1ex}
    {\textbf{Analysis workflow}}
  \vspace*{1ex}
    \begin{center}
      \includegraphics[width=.8\textwidth]{FortmannGrote2023_Figure_2.pdf}
    \end{center}
    \vspace{1ex}
    {\textbf{Webserver components}}
  \vspace*{1ex}
    \begin{center}
      \includegraphics[width=.8\textwidth]{rarefan_workflow.pdf}
    \end{center}
\end{block}%block
\printbibliography[title=]
\end{column}
\hspace*{\fill}%
%%% LEFT MAYOR COLUMN
\begin{column}{0.499\textwidth}
\begin{block}{Application\addheight}
  \vspace*{1ex}
  \textbf{REPINs in \textit{Stenotrophomonas maltophilia}~\cite{Balk2022}}
  \begin{center}
    \begin{figure}
      \tiny \centering
      \includegraphics[width=.88\textwidth]{FortmannGrote2023_Figure_3ab.pdf}
      \caption{Phylogeny of RAYT genes in \textit{S. maltophilia} genomes.
        Arrows indicate RAYTs in the reference genome. RAYTs are coloured
        according to their associated REPIN population in the reference genome.
        Grey color indicates REPIN population of query genome not present in the
        reference genome.}
    \end{figure}
    \vspace*{1ex}
    \begin{figure}
      \tiny \centering
      \includegraphics[width=.88\textwidth]{FortmannGrote2023_Figure_4.pdf}
      \caption{REPIN population sizes and conservation (reference strain Sm53)
        for REPIN groups according to 2 different associated ref. genome RAYTs. (A,B): 
        whole genome phylogenies (andi\cite{Haubold2015}) and corresponding
        RAYT and REPIN population sizes. (C,D): proportion of most common REPIN
        sequence in REPIN populations. Empty circles: unassociated REPINs.}
    \end{figure}
  \end{center}
  \vspace{1ex}
 \end{block}
 \begin{block}
   {Automated tests and release}
   \begin{center}
     \includegraphics[width=.6\textwidth]{Screenshot_2024-07-03_14-16-02.png}
     \includegraphics[width=.32\textwidth]{Screenshot_2024-07-03_14-17-01.png}
   \end{center}
 \end{block}
{\tiny\includegraphics[width=2cm]{by}\tiny\ Creative Commons Attribution 4.0 International License}
 \end{column}
\end{columns}
\end{frame}
\end{document}
