% Created 2024-03-12 Tue 11:11
% Intended LaTeX compiler: pdflatex
\documentclass[presentation]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\mode<beamer>{\usetheme{Madrid}}
\usepackage{biblatex}
\usepackage{tikz}
\usepackage{hyperxmp}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
\usetikzlibrary{arrows}
\usetheme{default}
\author{Carsten Fortmann-Grote\thanks{carsten.fortmann-grote@evolbio.mpg.de}}
\date{March 12 2024}
\title{Linked (Open) Data for Microbial Population Biology}
\titlegraphic{\includegraphics[height=0.3\textheight]{Luftbild.jpg}}
\hypersetup{
 pdfauthor={Carsten Fortmann-Grote},
 pdftitle={Linked (Open) Data for Microbial Population Biology},
 pdfkeywords={NFDI, Bio Imaging, Linked Data, Controlled Vocabulary, Knowledge Base},
 pdfsubject={NFDI4BIOIMAGE TA1 Jour Fixe},
 pdfdate={D:20240312110000Z},
 pdfcreator={Emacs 28.2 (Org mode 9.6.21)}, 
 pdflang={English}}
\begin{document}

\maketitle

\begin{block}{A short tour of the institute}
\tiny
\begin{columns}
\begin{column}{0.3\columnwidth}
\begin{block}{Evolutionary Genetics (D. Tautz (Emeritus))}
\begin{itemize}
\item Model organisms: Mus domesticus, mus musculus
\item Behavioural genomics
\item Population genetics
\end{itemize}
\begin{center}
\includegraphics[width=.9\linewidth]{2023-06-19_09-46-11_screenshot.png}
\end{center}
credits: \url{https://www.evolbio.mpg.de/3039130/group\_evolanimalbehpers}
\end{block}
\end{column}

\begin{column}{0.3\columnwidth}
\begin{block}{Microbial Population Biology (P. Rainey)}
\begin{itemize}
\item Model organism: Pseudomonas fluorescens, Bacillus sub.
\item Evolution of communities
\item Host-microbe interactions
\item Genetics
\end{itemize}
\begin{center}
\includegraphics[width=.9\linewidth]{sbw25.png}
\end{center}
credits: Theodosiou (left), Schwarz (middle), Grote (right)
\end{block}
\end{column}

\begin{column}{0.3\columnwidth}
\begin{block}{Theoretical Biology (A. Traulsen)}
\begin{itemize}
\item Population Structure and Game Theory
\item Metaorganisms
\item Cancer Evolution
\end{itemize}
\begin{center}
\includegraphics[width=.9\linewidth]{2023-06-19_10-07-46_screenshot.png}
\end{center}
Hindersin et al, PLOS Comp. Bio (2019) \url{10.1371/journal.pcbi.1004437}
\end{block}
\end{column}
\end{columns}
\end{block}


\section{Pseudomonas fluorescens SBW25 is a model organism for Microbial Population Biology}
\label{sec:org7a911da}
\begin{frame}[label={sec:org733f27c}]{Research topics in Microbial Population Biology}
\begin{itemize}
\item Bacterial communities as Darwinian entities
\item Antibiotica resistance
\item Bacteria and phages
\item Host-microbe interactions (plants, soil)
\item Long term evolution experiments
\item Adaptation to artificial selection pressures
\item Genetic bases for evolutionary processes
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org203a3f6}]{A typical MPB project}
\begin{itemize}
\item Wildtype clone and/or genetically modified strain(s)
\item Timelapse microscopy from growing microbial communities
\item Time resolved whole genome / core gene NGS data
\item Transcription profiles
\item Optical density measurements from plaque assays
\item Functional annotations
\end{itemize}
\alert{\alert{Need for integrated analysis of multiomics-multimodal data}}

"Find images, ELN entries for \(\Delta\) mreB strains and annotations for mreB"
\end{frame}


\begin{frame}[label={sec:orgfd9cad2}]{Integrating data the sparqling way}
\begin{block}{}
\begin{center}
\includegraphics[width=.9\linewidth]{PfluKnowledgeGraph_20240312.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:org6eaca57}]{Data integration of internal sources}
\begin{center}
\begin{tabular}{llll}
DB & API & LOD ready? & RDF conversion\\[0pt]
\hline
 &  &  & \\[0pt]
Tripal Genome DB & JSON-LD & yes & SPARQL\\[0pt]
 &  &  & SPARQL-anything\\[0pt]
 &  &  & virtualization\\[0pt]
 &  &  & \alert{JSON to RDF serialization}\\[0pt]
OMERO & JSON-LD & yes & omero-rdf\\[0pt]
OpenBIS & JSON & ??? & ??? ("semantic annotation")\\[0pt]
StrainDB & None & no & csv dump \(\rightarrow\) csv2rdf\\[0pt]
\end{tabular}
\end{center}
\end{frame}

\begin{frame}[label={sec:org43e9ac1}]{CSV2RDF}
\begin{center}
\includegraphics[width=.9\linewidth]{FW44NXZX0AAFTzg.jpeg}
\end{center}
\tiny Credits: Sarah Komla-Ebri
\end{frame}

\begin{frame}[label={sec:orgca9e1ce}]{Genome database}
\begin{columns}
\begin{column}{0.49\columnwidth}
\begin{block}{P.flu SBW25 Genome Database}
\begin{center}
\includegraphics[width=.9\linewidth]{Screenshot_2022-06-08_12-41-18.png}
\end{center}
\begin{center}
\includegraphics[width=.9\linewidth]{Screenshot_2022-07-05_21-58-08.png}
\end{center}
\end{block}
\end{column}
\begin{column}{0.49\columnwidth}
\begin{block}{Organism Database (Tripal)}
\begin{itemize}
\item Based on Drupal CMS
\item Implements Chado DB Scheme (SQL)
\item Converts genome annotation into searchable, cross-linked database
\item Integrates Genome Browser (JBrowse)
\item Supports OBO Ontologies \& Controlled Vocabularies (Sequence Ontology, Gene Ontology, \ldots{})
\item JSON-LD API \(\Rightarrow\) \alert{\alert{In principle}} ready for consumption by LOD clients
\end{itemize}
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:orga58a1a0},fragile]{Querying a JSON-LD API}
 \begin{block}{Direct query}
\tiny
\begin{verbatim}
SELECT distinct ?s ?p ?o where 
  {
    ?s a SO:0000316 .
    ?s ?p ?o .
}
\end{verbatim}

\begin{verbatim}
| s         | p                                               | o                                  |
|-----------+-------------------------------------------------+------------------------------------|
| CDS:11845 | http://www.w3.org/1999/02/22-rdf-syntax-ns#type | so:0000316                         |
| CDS:11845 | http://www.w3.org/2000/01/rdf-schema#label      | CDS:PFLU_0001-0                    |
| CDS:11845 | http://edamontology.org/data_2091               | 11845                              |
| CDS:11845 | https://schema.org/name                         | CDS:PFLU_0001-0                    |
| CDS:11845 | http://edamontology.org/data_0842               | CDS:PFLU_0001-0                    |
| CDS:11845 | http://purl.obolibrary.org/obo/OBI_0100026      | Pseudomonas fluorescens            |
| CDS:11845 | http://www.w3.org/2000/01/rdf-schema#type       | CDS                                |
| CDS:11845 | http://edamontology.org/data_2012               | CDS:11845/Sequence+coordinates     |
| CDS:11845 | http://purl.obolibrary.org/obo/OGI_0000021      | CDS:11845/location+on+map          |
| CDS:11845 | http://purl.obolibrary.org/obo/SBO_0000374      | CDS:11845/relationship             |
| CDS:11845 | http://purl.obolibrary.org/obo/SBO_0000554      | CDS:11845/database+cross+reference |
| CDS:11845 | http://semanticscience.org/resource/SIO_001166  | CDS:11845/annotation               |
\end{verbatim}
\end{block}

\begin{alertblock}{}
Object URIs are Graph URIs of linked json-ld documents.
Exploring the graph across documents requires dynamic generation of graph URIs, not supported in SPARQL1.1
\end{alertblock}
\end{frame}

\begin{frame}[label={sec:org18da68a},fragile]{Querying a JSON-LD API (cont.)}
 \begin{block}{SPARQL-Anything}
\tiny
\begin{verbatim}
PREFIX fx: <http://sparql.xyz/facade-x/ns/>
PREFIX xyz: <http://sparql.xyz/facade-x/data/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT distinct ?gene_name ?go  where {
  service <x-sparql-anything:>
  {
    fx:properties fx:location "http://pflu.evolbio.mpg.de/web-services/content/v0.1/CDS/12135";
                  fx:media-type "application/ld+json" .
    ?s a <http://www.sequenceontology.org/browser/current_svn/term/SO:0000316> .
    ?gs <http://pflu.evolbio.mpg.de/cv/lookup/local/gene> ?gene_name .
    ?gs <http://semanticscience.org/resource/SIO_001166> ?annotation .
  }
optional {
  service <x-sparql-anything:>
  {
    fx:properties fx:location ?annotation ;
    fx:media-type "application/ld+json" .
    ?annotation_s <http://semanticscience.org/resource/SIO_001080> ?vocab ;
                  <http://edamontology.org/data_2091> ?go_term ;
                  <https://schema.org/name> ?go_name ;
                  <http://purl.obolibrary.org/obo/IAO_0000115> ?definition .
    bind(concat(concat(?vocab, ":"), ?go_term) as ?go)
  }
}
}
\end{verbatim}
\end{block}
\end{frame}

\begin{frame}[label={sec:org8547d12},fragile]{Querying a JSON-LD API}
\tiny
\begin{verbatim}
| gene_name | go         | go_name                           |
|-----------+------------+-----------------------------------|
| wssH      | GO:0043806 | keto acid formate lyase activity  |
| wssH      | GO:0005886 | plasma membrane                   |
| wssH      | GO:0016021 | integral component of membrane    |
| wssH      | GO:0016746 | acyltransferase activity          |
| wssH      | GO:0042121 | alginic acid biosynthetic process |
\end{verbatim}
\begin{alertblock}{}
Still, this is quite complicated and probably not well supported by SPARQL query editors
\end{alertblock}
\end{frame}

\begin{frame}[label={sec:org19add7f},fragile]{Virtualization}
\begin{center}
\includegraphics[width=.6\textwidth]{VKG_schema.png}
\end{center}
\tiny{ Xiao et al (2019)}
\end{frame}
\begin{frame}[label={sec:org70cec46},fragile]{Example: VKG for the P.flu SBW25 Genome database}
 \begin{block}{Step 1: Bootstrapping with ontop-vkg}
\tiny

\begin{verbatim}
$> ontop bootstrap --db-url URL -p PROPS -t ONTOLOGY -m MAPPING
\end{verbatim}
\begin{verbatim}
<http://www.semanticweb.org/grotec/ontologies/2022/5/tripalv3/gene#dbxref_id> rdf:type owl:DatatypeProperty .
: mappingId	MAPPING-ID793
: target		_:ontop-bnode-534{feature_id}/{dbxref_id}/{organism_id}/{name}/{uniquename}/{residues}/{seqlen}/{md5checksum}/{type_id}/{is_analysis}/{is_obsolete}/{timeaccessioned}/{timelastmodified} a g:protein_coding_gene ;
g:protein_coding_gene#feature_id {feature_id}^^xsd:integer ;
g:protein_coding_gene#dbxref_id {dbxref_id}^^xsd:integer ;
g:protein_coding_gene#organism_id {organism_id}^^xsd:integer ;
g:protein_coding_gene#name {name}^^xsd:string ;
g:protein_coding_gene#uniquename {uniquename}^^xsd:string ;
g:protein_coding_gene#residues {residues}^^xsd:string ;
g:protein_coding_gene#seqlen {seqlen}^^xsd:integer ;
g:protein_coding_gene#md5checksum {md5checksum}^^xsd:string ;
g:protein_coding_gene#type_id {type_id}^^xsd:integer ;
g:protein_coding_gene#is_analysis {is_analysis}^^xsd:boolean ;
g:protein_coding_gene#is_obsolete {is_obsolete}^^xsd:boolean ;
g:protein_coding_gene#timeaccessioned {timeaccessioned}^^xsd:dateTime ;
g:protein_coding_gene#timelastmodified {timelastmodified}^^xsd:dateTime . 
: source		SELECT * FROM "chado"."protein_coding_gene"
\end{verbatim}
\end{block}
\end{frame}

\begin{frame}[label={sec:orgf1b8acb},fragile]{Example: VKG for the P.flu SBW25 Genome database}
 \begin{block}{Step 2: Cleanup}
\begin{itemize}
\item Highly redundant ontology: Every column label is translated into a data property \alert{\alert{for every table it occurs in}}
\end{itemize}

\begin{verbatim}
<tripalv3:gene#dbxref_id> rdf:type owl:DatatypeProperty .
<tripalv3:non_protein_coding#dbxref_id> rdf:type owl:DatatypeProperty .
\end{verbatim}
\end{block}

\begin{alertblock}{}
\begin{itemize}
\item Define single property to be applied across all tables.
\item Virtualization eventually worked but query execution time is slooooooooooooooooowwwwww
\end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}[label={sec:org24d5817}]{Flattening a deeply nested JSON-LD graph}
\begin{columns}
\begin{column}{0.48\columnwidth}
\begin{center}
\includegraphics[width=.9\linewidth]{2024-03-11_14-32-18_screenshot.png}
\end{center}
\end{column}

\begin{column}{.48\columnwidth}
\begin{block}{}
\begin{itemize}
\item rdflib for loading json-ld into graph structure
\item iteratively parses linked graph uris
\item dask for distributed computing
\item serialized to turtle format
\item -> 2.1 million Triples in ca. 12hrs
\end{itemize}
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org449f557}]{Our P.flu SBW25 RDF playground}
\begin{columns}
\begin{column}{.48\columnwidth}
\begin{center}
\includegraphics[height=.3\textheight]{2024-03-12_10-12-20_screenshot.png}
\end{center}

\begin{center}
\includegraphics[height=.3\textheight]{2024-03-12_10-13-27_screenshot.png}
\end{center}
\end{column}

\begin{column}{.48\columnwidth}
\begin{center}
\includegraphics[height=.3\textheight]{2024-03-12_10-17-30_screenshot.png}
\end{center}
\end{column}
\end{columns}
\end{frame}

\section{Example queries}
\label{sec:org9b33aff}

\begin{frame}[label={sec:orgc6d1d56},fragile]{Image, genotype, mpb number and straindb columns}
 \tiny
\begin{verbatim}

prefix ome_core: <http://www.openmicroscopy.org/rdf/2016-06/ome_core/>
prefix mpimap: <http://ome.evolbio.mpg.de/MapAnnotation/>
prefix mpiimg: <http://ome.evolbio.mpg.de/Image/>
prefix mpi: <http://ome.evolbio.mpg.de/>
prefix strains: <http://raineylab.evolbio.mpg.de/straindb/strains/>
prefix props: <http://raineylab.evolbio.mpg.de/straindb/properties/>
  select distinct ?img ?mpbn ?genotype ?phenotype where {
    ?img <http://www.wikidata.org/prop/direct/P180> ?ann .
    ?ann ome_core:Map ?map . 
    ?map ome_core:Key ?key .
    ?map ome_core:Value ?mpb .
    filter(?key in ("", "MPB")).

    bind(concat("MPB", ?mpb) as ?mpbn ).
  service  <http://micropop046.evolbio.mpg.de:3030/MPBStrainDB/sparql> {
      ?strain props:sId ?mpbn .
      ?strain props:genotype ?genotype .
      ?strain props:phenotype ?phenotype .
  }
         }
order by ?img
limit 7
\end{verbatim}


\begin{center}
\begin{tabular}{llll}
img & mpbn & genotype & phenotype\\[0pt]
\hline
\url{http://ome.evolbio.mpg.de/Image/6752778} & MPB15447 & PFLU0085 del 1309-1374 (del 437-458) & Wrinkly Spreader\\[0pt]
\url{http://ome.evolbio.mpg.de/Image/6752779} & MPB15447 & PFLU0085 del 1309-1374 (del 437-458) & Wrinkly Spreader\\[0pt]
\url{http://ome.evolbio.mpg.de/Image/6752780} & MPB15447 & PFLU0085 del 1309-1374 (del 437-458) & Wrinkly Spreader\\[0pt]
\url{http://ome.evolbio.mpg.de/Image/6752781} & MPB15456 & WsT AwsX delta229-261 & Wrinkly Spreader\\[0pt]
\url{http://ome.evolbio.mpg.de/Image/6752782} & MPB15456 & WsT AwsX delta229-261 & Wrinkly Spreader\\[0pt]
\url{http://ome.evolbio.mpg.de/Image/6752783} & MPB15456 & WsT AwsX delta229-261 & Wrinkly Spreader\\[0pt]
\url{https://ome.evolbio.mpg.de/Image/6752855} & MPB15455 & “22A1” WspF 150bpdel (delta 677-826) & Wrinkly Spreader\\[0pt]
\end{tabular}
\end{center}
\end{frame}

\section{GUI SPARQL clients}
\label{sec:org3af186d}
\begin{frame}[label={sec:org0a398c3},fragile]{SparNatural for visual query generation}
\url{file:///home/grotec/Sandbox/pflukg/index.html}

\begin{itemize}
\item Graphical query editor
\item Supports federation and customized data sources
\item No support (yet) for BIND and aggregations
\end{itemize}
\end{frame}

\section{The role of metadata standardization}
\label{sec:org5f6f954}
\begin{frame}[label={sec:org244bb15},fragile]{On omero-rdf}
 \tiny
\begin{verbatim}
select distinct ?key ?val where {
  # ?ann <http://www.wikidata.org/prop/direct/P180> ?img .
  ?ann ome_core:Map ?map . 
  ?map ome_core:Key ?key .
  ?map ome_core:Value ?val .
  }
\end{verbatim}

\begin{center}
\begin{tabular}{ll}
key & val\\[0pt]
\hline
Type & still\\[0pt]
Organism & Pseudomonas fluorescens\\[0pt]
Strain & SBW25\\[0pt]
MPB & 15447\\[0pt]
Parent & 0\\[0pt]
Investigation & Deep Learning for MPB\\[0pt]
Study & Segmentation and Classif\ldots{}5 colony morphotype\\[0pt]
Assay & 0085\\[0pt]
Phenotype & WS\\[0pt]
Genotype & 0085\\[0pt]
\end{tabular}
\end{center}


\begin{block}{}
\begin{itemize}
\item omero-rdf stores key, value as bnode property values.
\item would need key as property, value as property value
\item requires loading ontologies, controlled vocabs into omero and exposure in map annotations.
\end{itemize}
\end{block}
\end{frame}



\section{Tasks and contributions to NFDI4BI}
\label{sec:org4f1f0af}
\begin{frame}[label={sec:org18585bc}]{Tasks and contributions to NFDI4BI}
\begin{itemize}
\item P.flu SBW25 Knowledge Graph as a Use Case
\item Controlled vocabularies for microbial population biology and evolutionary cell biology
\item Contributions to linked open data software, clients, recommendations
\end{itemize}
\end{frame}
\end{document}
