% Created 2023-01-13 Fri 15:34
% Intended LaTeX compiler: pdflatex
\documentclass[bigger]{beamer}
% \documentclass[bigger,notes=only]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[style=chem-acs, maxnames=1]{biblatex}
\addbibresource{jabref.bib}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{lmodern}
\usepackage[babel=true]{microtype}
\AtEveryBibitem{\clearfield{doi}}
\AtEveryBibitem{\clearfield{DOI}}
\AtEveryBibitem{\clearfield{Doi}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{url}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{URL}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{editor}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{title}}{}}
\AtEveryBibitem{\clearfield{pubstate}}


\renewcommand{\footnotesize}{\scriptsize}

\usepackage{hyperref}
\usepackage{hyperxmp}
\mode<beamer>{\usetheme{Madrid}}
\usetheme{default}
\mode<presentation>

\input{resources/colors}
\definecolor{mpievolbio_green}{RGB}{0,124,119}
\setbeamercolor*{frametitle}{fg=white,bg=mpievolbio_green}
\setbeamercolor*{section in head/foot}{fg=white,bg=mpievolbio_green}
\setbeamercolor*{date in head/foot}{fg=white,bg=mpievolbio_green}
\setbeamercolor*{author in head/foot}{fg=white,bg=mpievolbio_green}
\setbeamercolor*{title in head/foot}{fg=white,bg=mpievolbio_green}
\setbeamercolor{structure}{bg=mpievolbio_green}
\setbeamercolor{block title}{fg=white,bg=structure.bg}
\setbeamercolor{block body}{use=block title,bg=block title.bg!10}
\setbeamercolor{title}{fg=white,bg=mpievolbio_green}
\setbeamercolor{itemize item}{fg=mpievolbio_green}
\setbeamercolor{itemize subitem}{fg=mpievolbio_green}
\setbeamercolor{enumerate item}{fg=mpievolbio_green}
\setbeamercolor{enumerate subitem}{fg=mpievolbio_green}
\setbeamercolor{description item}{fg=mpievolbio_green}
\setbeamertemplate{itemize item}[circle]
\setbeamertemplate{itemize subitem}[circle]
\setbeamertemplate{itemize subsubitem}[circle]
\setbeamercolor{alerted text}{fg=mpievolbio_green}

% Remove navigation symbols
\setbeamertemplate{navigation symbols}{}
% Position logo in lower right corner
\usepackage{tikz}
\usetikzlibrary{shapes.geometric}
\logo{%
  \begin{tikzpicture}[overlay, remember picture]
    \node[left=-0.18cm] at (0,0){
      \includegraphics[width=0.05\textwidth]{mpg_logo_FGgreen_BGalpha.png}
    };
  \end{tikzpicture}
}

\author[C. Fortmann-Grote]{\uline{Carsten Fortmann-Grote}, Julia Balk, Frederic Bertels}
\institute[MPI EvolBio]{Max Planck Institute for Evolutionary Biology}
\date[16 Jan 2023]{GWDG Life Science/Bioinformatics Workshop\\ 2023-01-16}
\title[RAREFAN]{RAREFAN: A publicly accessible bioinformatic pipeline on a cloud server - Implementation and limitations}
\subtitle{\href{https://doi.org/10.1101/2022.05.22.493013}{doi:10.1101/2022.05.22.493013} (bioRxiv)}
\hypersetup{
 pdfauthor={Carsten Fortmann-Grote, Julia Balk, Frederic Bertels},
 pdftitle={RAREFAN: A publicly accessible bioinformatic pipeline on a cloud server - Implementation and limitations},
 pdfkeywords={webserver, REPIN, RAYT, GWDG, flask, python, java},
 pdfsubject={GWDG Life Science/Bioinformatics Workshop 2023},
 pdfdate={D:20230116110000Z},
 pdfcreator={Emacs 27.1 (Org mode 9.6)}, 
 pdflang={English}
}
\begin{document}

\maketitle

\note{%
  Thank you very much for the introduction and for the opportunity to present
  our work at this workshop.
  As you will see, this work by itself is not a HPC application, indeed it is
  maybe even questionnable if it could benefit from HPC resources at least in
  its current state. If anything, it would be rather a usecase for High
  Throughput Computing but I'll put this difference aside for now.
  Nevertheless, it may serve as an example and motivating use
  case for making HPC/HTC resources available to bioinformatic services that are
  exposed
  to ``end users'' much like online services like blast.
}

\begin{frame}{Outline}
\tableofcontents
\end{frame}

\note{
  What I'll be presenting you now is a bioinformatic pipeline named RAREFAN for
  ``REPIN And RAYT Finder and Analyzer'' which is exposed to the general public
  as an online web service. I will briefly explain what REPINs and RAYTs are,
  and why they are interesting research subjects. I will then walk you through
  the utilization of our web service before I will discuss some details of
  implementation, performance, and then connect to possible future opportunities
  and ways that this and similar web services may benefit from HPC resources.
}

\section{Introduction: REPINs and RAYTs}
\label{sec:org9873f26}
\begin{frame}[label={sec:org9f95d64}]{REPINs and RAYTs~\footfullcite{Bertels2011,Bertels2017}}
\begin{block}{What are REPINs}
\begin{columns}
\begin{column}{0.34\columnwidth}
\begin{itemize}
\item Short (\textasciitilde{} 100bp)
\item Nested inverted repeats \(\rightarrow\) hairpins
\item Highly conserved
\item Unclear function
\end{itemize}
\end{column}
\begin{column}{0.64\columnwidth}
\begin{center}
\includegraphics[width=\textwidth]{repins_genomebrowserview-crop.pdf}
\end{center}
\end{column}
\end{columns}
\end{block}
\end{frame}
\note{
  REPINs are short (approx. 100bp long) palindromic sequences, that is, a nested
  inverted repeat. They are often
  found in bacterial genomes. Noteworthy is their high conservation for millions
  of years in individual species, yet their function is largely unknown. This
  makes them an interesting subject of evolutionary research at the
  microbiological and molecular scale.
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% \begin{frame}[label={sec:org9f95d64}]{REPINs and RAYTs}
% \begin{block}{REPINs are associated with RAYTs}
% \begin{center}
% \includegraphics[width=.9\linewidth]{repins_rayt_blocks-crop.pdf}
% \end{center}
% \end{block}
% \end{frame}
% \note{
%   Identifying REPINs in a given genomic sequence starts with the observation
%   that REPINs never occur in isolation but are (by definition) associated with a
%   RAYT gene in it's vicinity. This RAYT, a transposon, copies an existing REP
%   sequence and pastes it into another position, thereby helping the REPIN to
%   spread throughout the host genome.
%
% }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \begin{frame}[label={sec:org9f95d64}]{REPINs and RAYTs}
% \begin{block}{RAYTs replicate REPINs throughout the genome}
% \begin{columns}
% \begin{column}{0.34\columnwidth}
% \begin{itemize}
% \item Only in intergenic regions
% \item 100s of times in bacterial genomes
% \item Vicinity to RAYT genes
% \end{itemize}
% \end{column}
% \begin{column}{0.64\columnwidth}
% \begin{center}
% \includegraphics[width=.8\textwidth]{repins_in_pflusbw25.png}
% \end{center}
% \end{column}
% \end{columns}
% \end{block}
% \end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{frame}[label={sec:org9e066ef}]{REPIN Analysis as a Service}
\begin{block}{RAREFAN workflow}
\begin{center}
\includegraphics[width=.87\textwidth]{rarefan_workflow.pdf}
\end{center}
\end{block}
\end{frame}
\note{
  Researchers interested in annotating the noncoding regions of microbial
  genomes face the difficulty that REPINs are hard to reckognize and it requires
  in depth knowledge of the peculiar structure and genetic properties of REPINs.

  The workflow is depicted on this slide and without going through all the
  details I believe it becomes clear that it is a rather complex process that
  represents a substantial obstacle for non-experts.

  Therefore, and to enable researchers to identify REPINs in microbial genomes,
  we publish our algorithm as a web service which does not require any coding.
Let me now quickly walk you through the process of using our webservice which
  you find at the indicated URL. First, the user is asked to upload their
  genomes and (optional) RAYT proteins.
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{frame}[label={sec:orgd89a332}]{REPIN Analysis as a Service}
\begin{block}{RAREFAN:}
\begin{itemize}
\item facilitates REPIN identification in a no-code, easily accessible way
\item identifies RAYTs and REPINs in bacterial genomes
\item constructs phylogenies of RAYTs and REPINs across bacterial genomes
\item analyses RAYT and REPIN population statistics
\end{itemize}
\end{block}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% \section{Using RAREFAN}
% %%%
% \label{sec:org39366ad}
% \begin{frame}[label={sec:org8a8d42e}]{Using RAREFAN}
% \begin{block}{Homepage \url{http://rarefan.evolbio.mpg.de}}
% \begin{center}
% \includegraphics[width=.8\textwidth]{2023-01-13_10-15-20_screenshot.png}
% \end{center}
% \end{block}
% \end{frame}
% \note{
%   Let me now walk you quickly through the process of using our web service. On
%   the homepage, the user is asked to upload their genomes and (optionally) RAYT proteins.
%   
% }
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% % \begin{frame}{Using RAREFAN}
% % \begin{block}{Upload genome(s)}
% % \begin{center}
% % \includegraphics[width=.9\linewidth]{2023-01-13_10-17-56_screenshot.png}
% % \end{center}
% % \end{block}
% % end{frame}
% \begin{frame}{Using RAREFAN}
% \begin{block}{Set run options}
% \begin{center}
% \includegraphics[height=.8\textheight]{2023-01-13_10-18-54_screenshot.png}
% \end{center}
% \end{block}
% \end{frame}
% \note{
%   After uploading their sequences, the user than selects the run options. Among
%   other, these are minimum length and occurence of Seed sequences to initialize
%   the algorithm, the minimum distance between REPIN clusters and the distance
%   between RAYTs and REPINs to be considered associated. All parameters are
%   documented in little callouts and in the online manual. Users then submit
%   their run which puts it into the execution queue.
% }
% \begin{frame}{Using RAREFAN}
% \begin{block}{Monitor run progress}
% \begin{center}
% \includegraphics[height=.8\textheight]{2023-01-13_10-21-05_screenshot.png}
% \end{center}
% \end{block}
% \end{frame}
% \note{
%   While the run is sitting in the queue and while being executed, the results
%   page displays information about the job progress.
% }
% \begin{frame}{Using RAREFAN}
% \begin{block}{Run and results summary}
% \begin{center}
% \includegraphics[height=.8\textheight]{2023-01-13_10-29-38_screenshot.png}
% \end{center}
% \end{block}
% \end{frame}
% \note{
%   Upon completion, the result page displays some summary statistics and provides
%   links to browse, download, and visualize the data as well as a link that can
%   be used to rerun the same job with optionally changed parameters thus avoiding
%   to upload all submitted genomes again.
% }
% % \begin{frame}{Using RAREFAN}
% % \begin{block}{Visualize results: RAYT phylogeny}
% % \begin{center}
% % \includegraphics[width=.9\linewidth]{2023-01-13_10-24-40_screenshot.png}
% % \end{center}
% % \end{block}
% % \end{frame}
% \begin{frame}{Using RAREFAN}
% \begin{block}{Visualize results: REPIN tree and population size}
% \begin{center}
% \includegraphics[width=.9\linewidth]{2023-01-13_10-26-07_screenshot.png}
% \end{center}
% \end{block}
% \end{frame}
% \note{
%   Using the ``Plot Results'' link opens a new browser tab with a R Shiny App
%   displaying e.g. the REPIN phylogeny along with REPIN and RAYT population sizes.
% }
% % \begin{frame}{Using RAREFAN}
% % \begin{block}{Visualize results: Correlation plot}
% % \begin{center}
% % \includegraphics[width=.9\linewidth]{2023-01-13_10-27-24_screenshot.png}
% % \end{center}
% % \end{block}
% \end{frame}
%%%
\section{Hardware and Software}
\note{
  This concludes the walkthrough. I will now describe on a few slides the
  hardware- and software environment in which RAREFAN operates.
}
% \begin{frame}[label={sec:org2df0ce0}]{Software Stack}
% \begin{itemize}
% \item Webserver: apache2
% \item Web frontend: flask, dropzone.js
% \item DB Backend: MongoDB
% \item Scheduling: redis, python-rq
% \item Bioinformatic backends:
% \begin{itemize}
% \item java code "RepinEcology" \cite{Bertels2011, Bertels2022, Bertels2017a}
% \item Blast+ \cite{Camacho2009}
% \item MCL \cite{VanDongen2008}
% \item Andi \cite{Haubold2015}
% \item Muscle \cite{Edgar2004}
% \item PhyML3 \cite{Guindon2010}
% \end{itemize}
% \item Plotting: R (ggplot2, shiny)
% \end{itemize}
% \end{frame}
% \note{
%   The software stack consists of a apache web server backed by a flask app and a
%   mongo database. The flask app sends individual compute task to the redis
%   scheduling server which, upon availability of resources launches the
%   corresponding bioinformatic backend. The main part of RAREFAN is implemented
%   in java, complemented by system calls for alignments, clustering and
%   phylogenetic inference. The visualiation is written in R using ggplot2 and the
%   shiny app framework for online rendering.
% }
\begin{frame}[label={sec:org6dbd1c6}]{RAREFAN components}
\begin{center}
\includegraphics[width=.9\linewidth]{rarefan_workflow.pdf}
\end{center}
\end{frame}
\note{
  The various component are again shown on this chart: The user submits its data
  through the web form, requesting a new job to be processed. This request is
  handled by the flask app which enqueues the individual tasks on the redis
  server and writes the run parameters to the database. redis executes the
  backends and writes the result also to the database. Clicking the ``Plot''
  link fires up the Shiny App which serves the rendered visualization back to
  the user. Jobs are stored for 180 days in the database.
}

\begin{frame}[label={sec:org99352f8}]{Host server}
\begin{itemize}
\item GWDG Cloud Server (8 VCPUs, 16GB RAM)
\item Debian 10 GNU/Linux
\item 1 Redis job executed at a time
\item Java VM limited to 10 GB
\end{itemize}
\end{frame}
\note{
  The entire software stack is implemented on a rather moderate cloud server
  operated by GWDG. Due to its moderate resources we can currently only process
  one job at a time. For the current traffic on our site, this seems to be ok
  although we have seen server outages due to excessive memory usage.
}

\section{Some thoughts on Performance}
\begin{frame}[label={sec:org2d98982}]{Job execution timings}
\begin{columns}
\begin{column}{0.49\columnwidth}
\begin{block}{Wall time vs. number of submitted genomes}
\begin{center}
\includegraphics[width=\textwidth]{wall_time__vs__genomes.png}
\end{center}
\end{block}
\end{column}

\begin{column}{0.49\columnwidth}
\begin{block}{Wall time per genome and Mbase genome size}
\begin{center}
\includegraphics[width=\textwidth,height=.545\textheight]{normalized_wall_time__vs__species.png}
\end{center}
\end{block}
\end{column}
\end{columns}

\begin{block}{}
 $\approx$~9 sec per Mbase of submitted genome data
\end{block}
\end{frame}
\note{
  On this slide I show some timing results. The left panel displays the wall time to completion
  as a function of the number of genomes and for various bacterial species.
  Normalizing to the number of genomes and the genome size yields the data for
  the right panel from which we conclude that RAREFAN takes of the order 9
  seconds per Mb of submitted genome data. For a individual genome of 5-10 Mb ,
  this means a single genome job is completed in round about one minute. For
  multiple genomes, this duration extends accordingly. 

  The second question we asked ourselves is how well could our code could benefit from
  more resources, in particular more CPUs. 
}

\begin{frame}[label={sec:orge1df67f}]{Performance scales poorly with available CPUs}
\begin{center}
\includegraphics[width=.9\linewidth]{timings_neiss.png}
\end{center}
\end{frame}
\note{
  To this end, we measured the wall time to completion for a single reference
  dataset (Neisseria menigitidis and Neisseria ghonoroeae, consisting of 40 genomes) as a function of CPUs
  made available to the service. As you see the performance scale rather poorly,
  platooing off if the number of CPUs is increased beyond 4, whereas in
  principle it should be possible to achieve close to linear scaling up to 40
  CPUS (one CPU per genome). This indicates that
  the code spends a large fraction of time in the serial part. So, in it's
  current state, throwing more resources at RAREFAN would be of limited use
  except for the situation when more users access the service concurrently at
  which point we would have to support concurrent processing of jobs in our
  queue.

}


\begin{frame}[label={sec:org3b276e9}]{Outlook}
\begin{block}{}
\begin{itemize}
\item Plenty of headspace to improve backend's parallel performance
\item Concurrent processing of submitted jobs is currently not possible
\item Limited resources may become an issue when userbase increases
\end{itemize}
\(\Rightarrow\) More/better cloud resources?\\[0pt]
\(\Rightarrow\) Options to offload compute intensive parts to HPC/HTC, e.g. via HPC REST-API\\[0pt]
\(\Rightarrow\) Wrap RAREFAN in workflow engine (galaxy, nextflow, ???)
\end{block}
\end{frame}
\note{
  Nevertheless, we will put some of our resources into improving the RAREFAN
  backends, in particular the java part and having HPC/HTC resources may become
  interesting at a not too distanc point in the future. So the interesting
  question is how can HPC resources be made available to such a web based
  service like RAREFAN. Are there ways to offload compute intensive parts of the
  webservice
  to a cluster? Currently, my understanding is that GWDG's cloud servers are
  isolated from the HPC system, probably for good reasons. I hope that this
  presentation serves as an example illustrating that there may be needs for
  such a HPC/HTC service in the cloud in the future. Thank you very much.}

% \begin{frame}[label={sec:orgb20a4ca}]{The End}
% \alert{Thank you very much!}
% \end{frame}
\end{document}
