%\documentclass[t,handout]{beamer} % Use for static version without overlays.
\documentclass[t]{beamer} % Use for actual presentation

% Path to figure files.
\graphicspath{resources/figures}
%

%define theme
%\usetheme{mpievolbio}
%\usetheme{Madrid}
\usetheme{metropolis}
\setlength{\skip\footins}{10cm}
\setlength{\footskip}{0ex}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Packages
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\usepackage[labelformat=empty]{subfig}
\usepackage{etex}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{etex}
\usepackage{graphicx}
\usepackage{latexsym}
\usepackage{lipsum}
\usepackage{multirow}
\usepackage{pdfpages}
\usepackage{tikz}
\usepackage{ulem}
\usepackage{currfile}
\usepackage{glossaries}
\usepackage{lipsum}
\usepackage{listings}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% glossary
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\input{glossary}
\makeglossaries

\usepackage[labelformat=empty]{subfig}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% pgf/tikz config
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\usepackage{pgfplots}
\usepackage{tikz}
\pgfplotsset{width=7cm,compat=1.10}
\usetikzlibrary{shadings}
\usetikzlibrary{shapes.arrows}
\usetikzlibrary{positioning}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hyperref config
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[]{hyperref}
\usepackage {hyperxmp}
\hypersetup{%
    unicode=true,                                                 % non-Latin characters in Acrobat’s bookmarks
    pdftoolbar=true,                                              % show Acrobat’s toolbar?
    pdfmenubar=true,                                              % show Acrobat’s menu?
    pdffitwindow=false,                                           % window fit to page when opened
    pdfpagemode={FullScreen},
    pdfauthor={Carsten Fortmann-Grote},                                           % author
    pdftitle={Stochastic simulations of microbial populations and Bayesian parameter inference},   % title
    pdfsubject={AMCP Seminar, Physics Institute, Rostock University},                             % subject of the document
    pdfdate={D:20220621140000Z},
    pdfcreator={pdflatex},                                         % creator of the document
    pdfkeywords={Gillespie Algorithm, stochastic differential equations, Bayesian inference, genetic barcode sequencing},                                         % list of keywords
    pdfnewwindow=true,                                            % links in new PDF window
    colorlinks=true,                                              % false: boxed links; true: colored links
    linkcolor=blue,                                                % color of internal links (change box color with linkbordercolor)
    linkbordercolor=white,
    urlbordercolor=white,
    filebordercolor=white,
    citebordercolor=white,
    citecolor=blue,                                                % color of links to bibliography
    filecolor=blue,                                               % color of file links
    urlcolor=blue                                                 % color of external links
}

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% biblatex config
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\usepackage[style=chem-acs]{biblatex}
\addbibresource{jabref.bib}
%
\AtEveryBibitem{\ifentrytype{article}{\clearfield{doi}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{DOI}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{url}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{editor}}{}}
\AtEveryBibitem{\clearfield{pubstate}}
%Beamer-mode
\mode<presentation>
\setbeamercovered{transparent}
% No navigation symbols.
\setbeamertemplate{navigation symbols}{}
% \setbeamertemplate{footline}[text line]{\tiny{\currfilename}}
%
\usetikzlibrary{arrows.meta,shapes}
\tikzset{%
  >={Latex[width=2mm,length=2mm]},
  % Specifications for style of nodes:
            base/.style = {rectangle,
                           fill=blue!20,
                           rounded corners,
                           draw=black,
                           text centered, font=\sffamily},
  %activityStarts/.style = {base, fill=blue!30},
       %startstop/.style = {base, fill=red!30},
    %activityRuns/.style = {base, fill=green!30},
         process/.style = {base, fill=orange!15,
                           font=\sffamily},
         switch/.style = {base, diamond, aspect=2, fill=green!10, minimum width=0,minimum height=0},
}
%
%Userdefined colors
\input{resources/colors}
\definecolor{mpievolbio_green}{RGB}{0,124,119}
\setbeamercolor*{frametitle}{fg=white,bg=mpievolbio_green}
\setbeamercolor{alerted text}{%
  fg=mpievolbio_green
}
%title and author
\title[Stochastic simulations]{Stochastic simulations of microbial populations and Bayesian parameter inference}%
\author[C. Fortmann-Grote]{Carsten Fortmann-Grote}
\institute[MPI Evolutionary Biology]{%
  \includegraphics[width=0.5\textwidth]{mpieb_logo_FGgreen_BGalpha}%
}%
\date[]{AMCP Seminar, Institut f\"ur Physik\\ Rostock University\\   June 21 2022 }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The logo
\logo{%
    \includegraphics[width=0.05\textwidth]{mpg_logo_FGgreen_BGalpha}%
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% In dev mode, include only WIP slide
%\includeonly{slides/barcode_sim_results.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Begin document
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{document}
\setbeamerfont{footnote}{size=\tiny}
\begin{frame}%
  \titlepage%
\end{frame}
\begin{frame}{Outline}
  \begin{itemize}
  \item Microbial Population Biology with \textit{Pseudomonas fluorescens} SBW25
  \item Barcode sequencing for evolutionary tracking
  \item Stochastic simulations
  \item Inference of population growth parameters
  \item Results
  \item Summary
  \end{itemize}
\end{frame}
%
\section{Studying Microbial Population Biology with \textit{Pseudomonas fluorescens} SBW25}
\subsection{Evolution at the microscale}
\include{slides/evolution_at_the_microscale.tex}
\include{slides/lenski_ltee.tex}
\include{slides/pflu_plants.tex}
\include{slides/Rainey1998.tex}
% \include{slides/evolution_multicellularity.tex}
% \include{slides/ecological_scaffolding.tex}

\section{Tracking genotype lineages through barcode sequencing}
\include{slides/SBW25_phenotypes_mutations.tex}
\include{slides/loukas_exp_setup.tex}
\include{slides/fitness.tex}
\include{slides/loukas_barcode_tracking.tex}
\include{slides/example_barcodes.tex}
\include{slides/barcode_tracking.tex}
% 
\include{slides/barcode_shaking_vs_static.tex}
\include{slides/barcode_analysis_pipeline.tex}
%
\section{Stochastic simulations of barcoded populations}
%\include{slides/biosystem_modeling.tex}
% \include{slides/stochsaas.tex}
\include{slides/gillespie.tex}
\include{slides/barcode_ssa_with_mutations.tex}
\include{slides/barcode_sim_results.tex}
% \include{slides/parallel_ssa.tex}
% 
\section{Bayesian inference}
\include{slides/inference_of_LV_model_parameters}%
\include{slides/bayes.tex}%
% \include{slides/compmat_vs_fitness.tex}
%
\section{Preliminary results}
\include{slides/glv_inference_1lineage.tex}
\include{slides/barcode_glv_inference.tex}
% \include{slides/bartender_test.tex}
% \include{slides/bartender.tex}
% \include{slides/FitSeq.tex}
\include{slides/summary.tex}
%
%
%
\end{document}
